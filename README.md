# AndroidHomeCodingTask #


### What is this repository for? ###

* This repository is created for a home coding task for android position.
* Current version is 1.0


### How do I get set up? ###

* Clone project and open with Android Studio (or IntelliJ or any IDE supported gradle build).

### How to use and test? ###
* To Build and run project, just choose run app and build process will be called. Then choose a device to run sample on.

* To run unit tests in app, please choose Build Variants and change Test Artifact to "Unit Tests". After that, right click on test/java folder and choose "Run 'All Tests'"

### Contact ###

* Any problem please contact me via email xuanusm@gmail.com or skype xuanusm

### Note ###

* App is using some emoticons graphic from https://www.hipchat.com/emoticons just to make sample, won't be used in commercial.