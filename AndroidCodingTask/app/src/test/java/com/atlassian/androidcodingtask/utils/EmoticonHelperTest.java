package com.atlassian.androidcodingtask.utils;

import org.junit.Assert;

import java.util.List;

/**
 * Created by xuanusm on 7/25/2015.
 * Class created for testing emoticon helper class.
 */
public class EmoticonHelperTest {

    @org.junit.Test
    /**
     * Test case to test get mention list value in case null value is passed in.
     * Expect value of list mention to be get is 0.
     */
    public void testGetListMatchEmoticonPatternWithNullInput() throws Exception {
        List<String> emoticons = EmoticonsHelper.getListMatchEmoticonPattern(null);
        Assert.assertNotNull(emoticons);
        Assert.assertEquals(0, emoticons.size());
    }

    @org.junit.Test
    /**
     * Test case to test get emoticon list value in case empty value "" is passed in.
     * Expect value of list emoticon to be get is 0.
     */
    public void testGetListMatchEmoticonPatternWithEmpty() throws Exception {
        List<String> emoticons = EmoticonsHelper.getListMatchEmoticonPattern("");
        Assert.assertNotNull(emoticons);
        Assert.assertEquals(0, emoticons.size());
    }

    @org.junit.Test
    /**
     * Test case to test get emoticon list value in case multi blank value "                 " is passed in.
     * Expect value of list emoticon to be get is 0.
     */
    public void testGetListMatchEmoticonPatternWithBlankValue() throws Exception {
        List<String> emoticons = EmoticonsHelper.getListMatchEmoticonPattern("                     ");
        Assert.assertNotNull(emoticons);
        Assert.assertEquals(0, emoticons.size());
    }

    @org.junit.Test
    /**
     * Test case to test get emoticon list value in case only 1 emoticon is existed in string passed in.
     * Input value will be "this is a test to emoticon (tester) in it"
     * Expect value of list emoticon to be get is 1 in size and content is tester.
     */
    public void testGetListMatchEmoticonPatternWithSingleEmoticonValue() throws Exception {
        List<String> emoticons = EmoticonsHelper.getListMatchEmoticonPattern("this is a test to emoticon (tester) in it");
        Assert.assertNotNull(emoticons);
        Assert.assertEquals(1, emoticons.size());
        Assert.assertEquals("tester", emoticons.get(0));
    }

    @org.junit.Test
    /**
     * Test case to test get emoticon list value in case has many emoticons is existed in string passed in.
     * Input value will be "(tester1) want to test (tester2) to make sure (tester3) finish all work for (tester)"
     * Expect value of list emoticon to be get is 4 in size and content is tester1, tester2, tester3, tester.
     */
    public void testGetListMatchEmoticonPatternWithMultiEmoticonValue() throws Exception {
        List<String> emoticons = EmoticonsHelper.getListMatchEmoticonPattern("(tester1) want to test (tester2) to make sure (tester3) finish all work for (tester)");
        Assert.assertNotNull(emoticons);
        Assert.assertEquals(4, emoticons.size());
        Assert.assertEquals("tester1", emoticons.get(0));
        Assert.assertEquals("tester2", emoticons.get(1));
        Assert.assertEquals("tester3", emoticons.get(2));
        Assert.assertEquals("tester", emoticons.get(3));
    }

    @org.junit.Test
    /**
     * Test case to test get emoticon list value in case has many emoticons is existed in string passed in.
     * Input value will be "(test@er1) want to test (tes#ter2) to make sure (te!ster3) finish all work for (tester) @        "
     * Expect value of list emoticon to be get is 4 in size and content is tester1, tester2, tester3, tester.
     */
    public void testGetListMatchEmoticonPatternWithMultiEmoticonWithErrorValue() throws Exception {
        List<String> emoticons = EmoticonsHelper.getListMatchEmoticonPattern("(test@er1) want to test (tes#ter2) to make sure (te!ster3) finish all work for (tester) @        ");
        Assert.assertNotNull(emoticons);
        Assert.assertEquals(1, emoticons.size());
        Assert.assertEquals("tester", emoticons.get(0));
    }

    @org.junit.Test
    /**
     * Test case to test ignore empty value emoticon text.
     * Input value will be "() is not a valid emoticon"
     * Expect value of list emoticon to be get is 0 in size.
     */
    public void testGetListMatchEmoticonPatternWithEmptyEmoticonWithErrorValue() throws Exception {
        List<String> emoticons = EmoticonsHelper.getListMatchEmoticonPattern("() is not a valid emoticon");
        Assert.assertNotNull(emoticons);
        Assert.assertEquals(0, emoticons.size());
    }

    @org.junit.Test
    /**
     * Test case to test ignore large value emoticon text.
     * Input value will be "(largeamounttextemoticon) is not a valid emoticon"
     * Expect value of list emoticon to be get is 0 in size.
     */
    public void testGetListMatchEmoticonPatternWithLargeEmoticonWithErrorValue() throws Exception {
        List<String> emoticons = EmoticonsHelper.getListMatchEmoticonPattern("(largeamounttextemoticon) is not a valid emoticon");
        Assert.assertNotNull(emoticons);
        Assert.assertEquals(0, emoticons.size());
    }
}