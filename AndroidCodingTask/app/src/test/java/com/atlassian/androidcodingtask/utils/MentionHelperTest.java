package com.atlassian.androidcodingtask.utils;

import org.junit.Assert;

import java.util.List;

/**
 * Created by xuanusm on 7/25/2015.
 * Class created for testing mention helper class.
 */
public class MentionHelperTest {

    @org.junit.Test
    /**
     * Test case to test get mention list value in case null value is passed in.
     * Expect value of list mention to be get is 0.
     */
    public void testGetListMatchMetionPatternWithNullInput() throws Exception {
        List<String> mentions = MentionHelper.getListMatchMetionPattern(null);
        Assert.assertNotNull(mentions);
        Assert.assertEquals(0, mentions.size());
    }

    @org.junit.Test
    /**
     * Test case to test get mention list value in case empty value "" is passed in.
     * Expect value of list mention to be get is 0.
     */
    public void testGetListMatchMetionPatternWithEmpty() throws Exception {
        List<String> mentions = MentionHelper.getListMatchMetionPattern("");
        Assert.assertNotNull(mentions);
        Assert.assertEquals(0, mentions.size());
    }

    @org.junit.Test
    /**
     * Test case to test get mention list value in case multi blank value "                 " is passed in.
     * Expect value of list mention to be get is 0.
     */
    public void testGetListMatchMetionPatternWithBlankValue() throws Exception {
        List<String> mentions = MentionHelper.getListMatchMetionPattern("                     ");
        Assert.assertNotNull(mentions);
        Assert.assertEquals(0, mentions.size());
    }

    @org.junit.Test
    /**
     * Test case to test get mention list value in case only 1 mention is existed in string passed in.
     * Input value will be "this is a test to mention @tester in it"
     * Expect value of list mention to be get is 1 in size and content is tester.
     */
    public void testGetListMatchMetionPatternWithSingleMentionValue() throws Exception {
        List<String> mentions = MentionHelper.getListMatchMetionPattern("this is a test to mention @tester in it");
        Assert.assertNotNull(mentions);
        Assert.assertEquals(1, mentions.size());
        Assert.assertEquals("tester", mentions.get(0));
    }

    @org.junit.Test
    /**
     * Test case to test get mention list value in case has many mentions is existed in string passed in.
     * Input value will be "@tester1 want to test @tester2 to make sure @tester3 finish all work for @tester"
     * Expect value of list mention to be get is 4 in size and content is tester1, tester2, tester3, tester.
     */
    public void testGetListMatchMetionPatternWithMultiMentionValue() throws Exception {
        List<String> mentions = MentionHelper.getListMatchMetionPattern("@tester1 want to test @tester2 to make sure @tester3 finish all work for @tester");
        Assert.assertNotNull(mentions);
        Assert.assertEquals(4, mentions.size());
        Assert.assertEquals("tester1", mentions.get(0));
        Assert.assertEquals("tester2", mentions.get(1));
        Assert.assertEquals("tester3", mentions.get(2));
        Assert.assertEquals("tester", mentions.get(3));
    }

    @org.junit.Test
    /**
     * Test case to test get mention list value in case has many mentions is existed in string passed in.
     * Input value will be "@test@er1 want to test @tes#ter2 to make sure @te!ster3 finish all work for @@@tester @        "
     * Expect value of list mention to be get is 4 in size and content is tester1, tester2, tester3, tester.
     */
    public void testGetListMatchMetionPatternWithMultiMentionWithErrorValue() throws Exception {
        List<String> mentions = MentionHelper.getListMatchMetionPattern("@test@er1 want to test @tes#ter2 to make sure @te!ster3 finish all work for @@@tester @        ");
        Assert.assertNotNull(mentions);
        Assert.assertEquals(5, mentions.size());
        Assert.assertEquals("test", mentions.get(0));
        Assert.assertEquals("er1", mentions.get(1));
        Assert.assertEquals("tes", mentions.get(2));
        Assert.assertEquals("te", mentions.get(3));
        Assert.assertEquals("tester", mentions.get(4));
    }
}