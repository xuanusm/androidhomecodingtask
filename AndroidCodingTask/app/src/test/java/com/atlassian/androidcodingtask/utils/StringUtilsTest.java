package com.atlassian.androidcodingtask.utils;

import org.junit.Assert;

/**
 * Created by dell on 7/25/2015.
 * Class created for testing string util class.
 */
public class StringUtilsTest {
    @org.junit.Test
    /**
     * Test case 1, pass in null value and expect to receive true from method StringUtils.isEmpty
     */
    public void testStringIsEmptyWithNull() throws Exception {
        Assert.assertEquals(true, StringUtils.isEmpty(null));
    }

    @org.junit.Test
    /**
     * Test case 2, pass in empty value "" and expect to receive true from method StringUtils.isEmpty
     */
    public void testStringIsEmptyWithEmptyValue() throws Exception {
        Assert.assertEquals(true, StringUtils.isEmpty(""));
    }

    @org.junit.Test
    /**
     * Test case 3, pass in value with multi blank "         " and expect to receive true from method StringUtils.isEmpty
     */
    public void testStringIsEmptyWithMultiBlankValue() throws Exception {
        Assert.assertEquals(true, StringUtils.isEmpty("        "));
    }

    @org.junit.Test
    /**
     * Test case 4, pass in a random string value and expect receive false value from method StringUtils.isEmpty.
     */
    public void testStringIsNotEmpty() throws Exception {
        Assert.assertEquals(false, StringUtils.isEmpty("not empty string"));
    }
}
