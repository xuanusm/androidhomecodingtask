package com.atlassian.androidcodingtask.utils;

import org.junit.Assert;

import java.util.List;

/**
 * Created by dell on 7/25/2015.
 * Class created for testing url helper class.
 */
public class UrlHelperTest {
    @org.junit.Test
    /**
     * Test case 1, pass in null value and expect to return empty url list result.
     */
    public void testGetListMatchUrlWithNullInput() throws Exception {
        List<String> urls = UrlHelper.getListMatchUrlPattern(null);
        Assert.assertNotNull(urls);
        Assert.assertEquals(0, urls.size());
    }

    @org.junit.Test
    /**
     * Test case 2, pass in "" value and expect to return empty url list result.
     */
    public void testGetListMatchUrlWithEmptyInput() throws Exception {
        List<String> urls = UrlHelper.getListMatchUrlPattern("");
        Assert.assertNotNull(urls);
        Assert.assertEquals(0, urls.size());
    }

    @org.junit.Test
    /**
     * Test case 3, pass in "            " value and expect to return empty url list result.
     */
    public void testGetListMatchUrlWithMultiblankInput() throws Exception {
        List<String> urls = UrlHelper.getListMatchUrlPattern("            ");
        Assert.assertNotNull(urls);
        Assert.assertEquals(0, urls.size());
    }

    @org.junit.Test
    /**
     * Test case 4, this is a summarization of many happy case with input url is correct.
     */
    public void testGetListMatchUrlWithCorrectCasesInput() throws Exception {
        List<String> urls = UrlHelper.getListMatchUrlPattern("http://www.google.com.vn www.google.com.vn " +
                "google.com.vn http://google.com.vn https://testing.url https://google.com.vn");
        Assert.assertNotNull(urls);
        Assert.assertEquals(6, urls.size());
        Assert.assertEquals("http://www.google.com.vn", urls.get(0));
        Assert.assertEquals("www.google.com.vn", urls.get(1));
        Assert.assertEquals("google.com.vn", urls.get(2));
        Assert.assertEquals("http://google.com.vn", urls.get(3));
        Assert.assertEquals("https://testing.url", urls.get(4));
        Assert.assertEquals("https://google.com.vn", urls.get(5));
    }

    @org.junit.Test
    /**
     * Test case 5, this is a summarization of many unhappy case with input url is incorrect.
     */
    public void testGetListMatchUrlWithInCorrectCasesInput() throws Exception {
        List<String> urls = UrlHelper.getListMatchUrlPattern("go0gle.@om a.b a.b.c http://? http://?.@.0");
        Assert.assertNotNull(urls);
        Assert.assertEquals(0, urls.size());
    }
}
