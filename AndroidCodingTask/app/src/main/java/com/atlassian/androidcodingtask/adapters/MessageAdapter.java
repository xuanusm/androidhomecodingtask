package com.atlassian.androidcodingtask.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.atlassian.androidcodingtask.R;
import com.atlassian.androidcodingtask.models.MessageContents;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xuanusm on 7/24/2015.
 * Adapter to display message list and its content on UI.
 */
public class MessageAdapter extends RecyclerView.Adapter<MessageHolder> {
    private static final String TAG = MessageAdapter.class.getCanonicalName();

    private Context context;
    private List<MessageContents> messageList;

    public MessageAdapter(Context context, List<MessageContents> messages) {
        this.context = context;
        if (messages != null && !messages.isEmpty())
            this.messageList = messages;
        else
            this.messageList = new ArrayList<>();
    }

    @Override
    public MessageHolder onCreateViewHolder(ViewGroup parent, int i) {
        Log.i(TAG, "onCreateViewHolder");
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.message_item, parent, false);
        return new MessageHolder(v);
    }

    @Override
    public void onBindViewHolder(MessageHolder messageHolder, int i) {
        Log.i(TAG, "onBindViewHolder");
        messageHolder.getTvOriginal().setText(context.getResources().getString(R.string.original_value)
                + messageList.get(i).getOriginalMessage());
        messageHolder.getTvComponents().setText(context.getResources().getString(R.string.return_value)
                + messageList.get(i).toString());

    }

    @Override
    public int getItemCount() {
        Log.i(TAG, "getItemCount");
        return messageList.size();
    }

    /**
     * Add new message content into list and notify adapter to render layout again.
     * @param newMessageContents new message content will be displayed on screen.
     */
    public void addMessageContent(MessageContents newMessageContents) {
        Log.i(TAG, "addMessageContent");
        if (newMessageContents == null) return;

        if (messageList == null) {
            messageList = new ArrayList<>();
        }

        messageList.add(newMessageContents);
        notifyItemInserted(messageList.size() - 1);
    }
}
