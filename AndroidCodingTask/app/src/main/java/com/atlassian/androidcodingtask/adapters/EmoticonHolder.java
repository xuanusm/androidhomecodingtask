package com.atlassian.androidcodingtask.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.atlassian.androidcodingtask.R;
import com.atlassian.androidcodingtask.controls.EmoticonGifView;

/**
 * Created by xuanusm on 7/24/2015.
 * Holder for message list.
 */
public class EmoticonHolder extends RecyclerView.ViewHolder {
    private static final String TAG = EmoticonHolder.class.getCanonicalName();

    private ImageView imgEmoticon;
    private EmoticonGifView imgGifEmoticon;

    public EmoticonHolder(View itemView) {
        super(itemView);
        Log.i(TAG, "create message holder");

        imgEmoticon = (ImageView) itemView.findViewById(R.id.imgEmoticon);
        imgGifEmoticon = (EmoticonGifView) itemView.findViewById(R.id.imgGifEmoticon);
    }

    public ImageView getImgEmoticon() {
        return imgEmoticon;
    }

    public EmoticonGifView getImgGifEmoticon() {
        return imgGifEmoticon;
    }
}
