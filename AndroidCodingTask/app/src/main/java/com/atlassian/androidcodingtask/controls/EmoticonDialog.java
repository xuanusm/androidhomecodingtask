package com.atlassian.androidcodingtask.controls;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.atlassian.androidcodingtask.R;
import com.atlassian.androidcodingtask.adapters.EmoticonAdapter;
import com.atlassian.androidcodingtask.constants.AppConstants;
import com.atlassian.androidcodingtask.listeners.EmoticonListener;

/**
 * Created by xuanusm on 7/25/2015.
 * Dialog created to display list of emoticons so user can choose to use.
 */
public class EmoticonDialog extends Dialog {
    private static final String TAG = EmoticonDialog.class.getCanonicalName();
    private EmoticonListener listener;

    public EmoticonDialog(Context context, EmoticonListener listener) {
        super(context);
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.emoticon_grid_layout);
        setTitle(R.string.emoticon_dialog_title);

        RecyclerView lvEmoticons = (RecyclerView) findViewById(R.id.lvEmoticons);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this.getContext(), AppConstants.NUMBER_EMOTICON_COLUMN);
        lvEmoticons.setLayoutManager(layoutManager);
        EmoticonAdapter adapter = new EmoticonAdapter(getContext(), listener);
        lvEmoticons.setAdapter(adapter);
    }
}
