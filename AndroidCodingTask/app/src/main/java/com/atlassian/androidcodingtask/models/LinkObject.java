package com.atlassian.androidcodingtask.models;

import com.atlassian.androidcodingtask.utils.UrlHelper;
import com.google.gson.annotations.Expose;

/**
 * Model created to store information related to url after parse from input content.
 * Now it has original link and the title of that link.
 * Created by xuanusm on 7/24/2015.
 */
public class LinkObject {
    @Expose
    private String link;
    @Expose
    private String title;

    public LinkObject(String link) {
        this.link = link;
        this.title = UrlHelper.getTitleFromUrl(this.link);
    }

    public LinkObject(String link, String title) {
        this.link = link;
        this.title = title;
    }

    public String getLink() {
        return link;
    }
}
