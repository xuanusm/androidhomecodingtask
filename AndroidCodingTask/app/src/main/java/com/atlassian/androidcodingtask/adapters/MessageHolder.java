package com.atlassian.androidcodingtask.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.atlassian.androidcodingtask.R;

/**
 * Created by xuanusm on 7/24/2015.
 * Holder for message list.
 */
public class MessageHolder extends RecyclerView.ViewHolder {
    private static final String TAG = MessageHolder.class.getCanonicalName();

    private TextView tvOriginal;
    private TextView tvComponents;

    public MessageHolder(View itemView) {
        super(itemView);
        Log.i(TAG, "create message holder");

        tvOriginal = (TextView) itemView.findViewById(R.id.tvOriginal);
        tvComponents = (TextView) itemView.findViewById(R.id.tvComponents);
    }

    public TextView getTvOriginal() {
        return tvOriginal;
    }

    public TextView getTvComponents() {
        return tvComponents;
    }
}
