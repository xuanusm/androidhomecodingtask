package com.atlassian.androidcodingtask.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;

import com.atlassian.androidcodingtask.R;
import com.atlassian.androidcodingtask.constants.AppConstants;

/**
 * Welcome screen that will be closed after 2 seconds or by touching from user.
 * Created by xuanusm on 7/24/2015.
 */
public class SplashScreen extends Activity {
    private static final String TAG = SplashScreen.class.getCanonicalName();
    private boolean mActive = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        final int sleepTime = 100;
        Thread splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;
                    while(mActive && (waited < AppConstants.SPLASHSCREEN_TIME)) {
                        sleep(sleepTime);
                        if(mActive) {
                            waited += sleepTime;
                        }
                    }
                } catch(InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    finish();
                    Intent intent = new Intent(SplashScreen.this, TextInputActivity.class);
                    startActivity(intent);
                }
            }
        };
        splashTread.start();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            mActive = false;
        }
        return true;
    }
}
