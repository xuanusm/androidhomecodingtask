package com.atlassian.androidcodingtask.utils;

import android.util.Log;

import com.atlassian.androidcodingtask.constants.AppConstants;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by xuanusm on 7/24/2015.
 * Helper created to handle functions related to mention like search matching mention pattern on given string.
 */
public final class MentionHelper {
    private static final String TAG = MentionHelper.class.getCanonicalName();

    /**
     * Get list string pieces that is an mention existing in original message.
     * The rule for checking metion is it start with '@' and end with non-word character.
     * For example:
     * if original message is '@someone is using @user message' the list of mentions will be 'someone' and 'user'
     * @param originalMessage original message data to check existing mentions.
     * @return list of mentions contained in original message.
     */
    public static List<String> getListMatchMetionPattern(String originalMessage) {
        Log.i(TAG, "getListMatchMetionPattern");
        List<String> result = new ArrayList<>();
        if (StringUtils.isEmpty(originalMessage)) return result;

        Pattern mentionPattern = Pattern.compile(AppConstants.MENTION_PATTERN);
        Matcher m = mentionPattern.matcher(originalMessage);
        while (m.find()) {
            result.add(m.group(2));
        }

        return result;
    }
}
