package com.atlassian.androidcodingtask.listeners;

/**
 * Created by xuanusm on 7/25/2015.
 * Listener created to listen to emoticon selected event and execute to display it on composer.
 */
public interface EmoticonListener {
    /**
     * Api to handle displaying selected emoticon from given dialog.
     * @param emoticonText text of emoticon to display on UI.
     * @return string value to use after append emoticon text.
     */
    String displaySelectedEmoticon(String emoticonText);
}
