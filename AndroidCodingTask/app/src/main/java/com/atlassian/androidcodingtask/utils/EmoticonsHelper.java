package com.atlassian.androidcodingtask.utils;

import android.content.Context;
import android.util.Log;

import com.atlassian.androidcodingtask.R;
import com.atlassian.androidcodingtask.constants.AppConstants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by xuanusm on 7/24/2015.
 * Helper class created to handle functions related to emoticon like search matching emoticon pattern, get emoticon list...
 */
public final class EmoticonsHelper {
    private static final String TAG = EmoticonsHelper.class.getCanonicalName();

    private static List<String> emoticonList = new ArrayList<>();

    /**
     * Get list string pieces that is an emoticon existing in original message.
     * The rule for checking emticon is it start with '(' and end with ')', the length of emoticon is from 1 to 15.
     * For example:
     * if original message is '(someone) is using (user) message' the list of emoticons will be 'someone' and 'user'
     * @param originalMessage original message data to check existing emoticons.
     * @return list of emoticons contained in original message.
     */
    public static List<String> getListMatchEmoticonPattern(String originalMessage) {
        Log.i(TAG, "getListMatchEmoticonPattern");
        List<String> result = new ArrayList<>();
        if (StringUtils.isEmpty(originalMessage)) return result;

        Pattern emoticonPattern = Pattern.compile(AppConstants.EMOTICON_PATTERN);
        Matcher m = emoticonPattern.matcher(originalMessage);
        while (m.find()) {
            result.add(m.group(2));
        }

        return result;
    }

    /**
     * get the drawable resource id of the given emoticon name.
     * @param context app context to get resource.
     * @param emoticonName name of emoticon which want to have drawble.
     * @return id of drawable resource that match with emoticon name. -1 will be returned in case we cannot find any resource
     * that match the input.
     */
    public static int getEmoticonResourceByName(Context context, String emoticonName) {
        Log.i(TAG, "enter getEmoticonResourceByName");
        if (StringUtils.isEmpty(emoticonName) && !isInSupportedEmoticonList(context, emoticonName)) return -1;

        Log.i(TAG, "emoticon name is supported: " + emoticonName);
        return context.getResources().getIdentifier(emoticonName, AppConstants.DRAWBLE_RESOURCE, context.getPackageName());
    }

    /**
     * Method created to check is emoticon text is in the supported emoticon list in app.
     * @param context app context to get resource.
     * @param emoticonText emoticon text to check.
     * @return true if input emoticon text in in supported list. Otherwise is false.
     */
    public static boolean isInSupportedEmoticonList(Context context, String emoticonText) {
        Log.i(TAG, "enter isInSupportedEmoticonList");
        return getEmoticonList(context).contains(emoticonText);
    }

    /**
     * Get list emoticons sample list in string resource that can have art-work to display.
     * @param context context to use.
     * @return list of emoticon text from resource.
     */
    public static List<String> getEmoticonList(Context context) {
        if (emoticonList == null || emoticonList.isEmpty()) {
            String emoticonString = context.getResources().getString(R.string.emoticon_list);
            emoticonList = Arrays.asList(emoticonString.split(AppConstants.STRING_SEPARATOR));
        }
        return emoticonList;
    }
}
