package com.atlassian.androidcodingtask.models;

import android.util.LruCache;

import org.jsoup.nodes.Document;

/**
 * Created by xuanusm on 7/28/2015.
 * Cache to store link info to avoid getting same info from same link
 */
public class LinkLruCache extends LruCache<String, Document> {

    /**
     * @param maxSize for caches that do not override {@link #sizeOf}, this is
     *                the maximum number of entries in the cache. For all other caches,
     *                this is the maximum sum of the sizes of the entries in this cache.
     */
    public LinkLruCache(int maxSize) {
        super(maxSize);
    }

    @Override
    protected int sizeOf(String key, Document value) {
        return value.toString().getBytes().length;
    }
}
