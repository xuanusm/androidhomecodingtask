package com.atlassian.androidcodingtask.utils;

import android.util.Log;

import com.atlassian.androidcodingtask.constants.AppConstants;
import com.atlassian.androidcodingtask.models.LinkLruCache;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by xuanusm on 7/24/2015.
 * Helper class created to do functions related to url like search matching url pattern on given string
 * or check validation of given url.
 */
public final class UrlHelper {
    private static final String TAG = UrlHelper.class.getCanonicalName();

    private static LinkLruCache urlCache = new LinkLruCache(AppConstants.CACHE_SIZE);

    /**
     * Method created to get title from given url.
     * It will used Jsoup library to parse the html content.
     * @param url the link that we want to get its title
     * @return title of the given link if it existed. Otherwise an empty string will be returned.
     */
    public static String getTitleFromUrl(String url) {
        Log.i(TAG, "getTitleFromUrl");
        String result = "";

        // if given url is empty, do not to handle anything.
        // to check is given url has valid format.
        if (StringUtils.isEmpty(url) || !isValidUrl(url)) return result;

        Log.i(TAG, "ready to get title from valid url " + url);
        try {
            String preHandleUrl = preHandleUrl(url);
            Document doc = urlCache.get(preHandleUrl);
            if (doc == null) {
                doc = Jsoup.connect(preHandleUrl).get();
                if (doc != null) {
                    urlCache.put(preHandleUrl, doc);
                }
            }
            if (doc != null)
                result = doc.title();
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "problem when trying to get title from " + url);
        }

        return result;
    }

    /**
     * handle url before connect and get it content.
     * If url does not contain http://, we will append it as prefix
     * @param originalUrl original value.
     * @return full url.
     */
    private static String preHandleUrl(String originalUrl) {
        String result = originalUrl;

        Matcher m = Pattern.compile(AppConstants.PROTOCOL_SUPPORT).matcher(originalUrl);
        if (!m.find()) {
            result = AppConstants.PROTOCOL_DEFAULT + result;
        }

        return result;
    }

    /**
     * Method created to check is given url valid or not.
     * @param url link to check.
     * @return true if the given url is valid in link format. Otherwise method will return false.
     */
    private static boolean isValidUrl(String url) {
        Log.i(TAG, "isValidUrl");
        return !StringUtils.isEmpty(url) && AppConstants.HTTP_WEB_URL.matcher(url).matches();

    }

    /**
     * Get list string pieces that is an url existing in original message.
     * @param originalMessage original message data to check existing urls.
     * @return list of urls contained in original message.
     */
    public static List<String> getListMatchUrlPattern(String originalMessage) {
        Log.i(TAG, "getListMatchMetionPattern");
        List<String> result = new ArrayList<>();
        if (StringUtils.isEmpty(originalMessage)) return result;

        Matcher m = AppConstants.HTTP_WEB_URL.matcher(originalMessage);
        while (m.find()) {
            result.add(m.group(1));
        }

        return result;
    }
}
