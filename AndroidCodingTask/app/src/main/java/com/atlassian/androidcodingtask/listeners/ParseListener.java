package com.atlassian.androidcodingtask.listeners;

/**
 * Created by xuanusm on 7/24/2015.
 * Listener created to listen to parse process and execute function before and after process finished.
 */
public interface ParseListener {
    /**
     * handle before parse process is started.
     */
    void preParsing();

    /**
     * handle after parse process is finished or cancelled.
     */
    void finishParsing();
}
