package com.atlassian.androidcodingtask.controls;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by xuanusm on 7/25/2015.
 * A custom view to display gif type emoticon.
 */
public class EmoticonGifView extends ImageView {
    private final static String TAG = EmoticonGifView.class.getCanonicalName();

    private GifDecoder gifDecoder = null;
    private Bitmap tmpBitmap;

    private final Handler handler = new Handler();

    private static final int MAX_RUNNING_FRAMES = 50;

    private Thread thread;

    private final Runnable updateResults = new Runnable() {

        public void run() {
        if (tmpBitmap != null && !tmpBitmap.isRecycled()) {
            EmoticonGifView.this.setImageBitmap(tmpBitmap);
        }
        }

    };

    public EmoticonGifView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void initialize(InputStream stream) throws IOException {
        gifDecoder = new GifDecoder();
        gifDecoder.read(stream);
    }

    public synchronized void startRendering() {
        Log.i(TAG, "startRendering");
        if (thread != null && thread.isAlive()) {
            Log.i(TAG, "startRendering: thread already running");
            return;
        }

        thread = new Thread(new Runnable() {
            public void run() {
                final int n = gifDecoder.getFrameCount();
                final int ntimes = gifDecoder.getLoopCount();
                int repetitionCounter = 0;
                int runningFrames = 0;
                do {
                    for (int i = 0; i < n; i++) {
                        if (Thread.interrupted()) {
                            return;
                        }

                        tmpBitmap = gifDecoder.getFrame(i);
                        int t = gifDecoder.getDelay(i);
                        handler.post(updateResults);
                        try {
                            Thread.sleep(t);
                        } catch (InterruptedException | IllegalArgumentException e) {
                            return;
                        }
                    }
                    if (ntimes != 0) {
                        repetitionCounter++;
                    }

                    if (++runningFrames == MAX_RUNNING_FRAMES) {
                        return;
                    }

                } while (repetitionCounter <= ntimes);
            }
        });
        thread.start();
    }

    public synchronized void stopRendering() {
        Log.i(TAG, "stopRendering");
        if (thread != null) {
            thread.interrupt();
            thread = null;
        }
    }
}
