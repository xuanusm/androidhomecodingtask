package com.atlassian.androidcodingtask.utils;

/**
 * Created by xuanusm on 7/25/2015.
 * An utility class to help check things related to String value.
 */
public final class StringUtils {
    /**
     * Method to check a string input is empty or not.
     * Null is considered as empty.
     * '' is considered as empty.
     * '       ' is also considered as empty.
     * @param originalValue input string to check.
     * @return true if input string is related to 1 of 3 cases listed. Otherwise is false.
     */
    public static boolean isEmpty(String originalValue) {
        return originalValue == null || "".equals(originalValue.trim());
    }
}
