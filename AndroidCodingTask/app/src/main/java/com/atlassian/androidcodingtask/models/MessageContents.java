package com.atlassian.androidcodingtask.models;

import android.content.Context;
import android.util.Log;

import com.atlassian.androidcodingtask.utils.ConnectionHelper;
import com.atlassian.androidcodingtask.utils.EmoticonsHelper;
import com.atlassian.androidcodingtask.utils.MentionHelper;
import com.atlassian.androidcodingtask.utils.StringUtils;
import com.atlassian.androidcodingtask.utils.UrlHelper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xuanusm on 7/24/2015.
 * Main model created to store information parsed.
 * From the raw input, message will be parsed and put into different variables related to emoticon, mention or link.
 */
public class MessageContents {
    private static final String TAG = MessageContents.class.getCanonicalName();

    private String originalMessage;
    @Expose
    private List<String> emoticons;
    @Expose
    private List<String> mentions;
    @Expose
    private List<LinkObject> links;

    /**
     * Constructor that receive message input from user.
     * We will handle parsing original message to find out parts of emoticon, mention and link.
     * @param originalMessage original message input from user.
     */
    public MessageContents(Context context, String originalMessage) {
        Log.i(TAG, "enter message constructor");

        this.originalMessage = originalMessage;
        emoticons = new ArrayList<>();
        mentions = new ArrayList<>();
        links = new ArrayList<>();
        if (!StringUtils.isEmpty(originalMessage)) {
            Log.i(TAG, "Original message: " + originalMessage);

            try {
                // parse the original message to have part of data that we want.
                emoticons.addAll(EmoticonsHelper.getListMatchEmoticonPattern(originalMessage));
                mentions.addAll(MentionHelper.getListMatchMetionPattern(originalMessage));
                List<String> urls = UrlHelper.getListMatchUrlPattern(originalMessage);
                boolean isConnected = ConnectionHelper.isConnectingToInternet(context);
                for (String url : urls) {
                    links.add(isConnected ? new LinkObject(url) : new LinkObject(url, ""));
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.i(TAG, "Exception occur while parsing message " + originalMessage);
            }
        }
    }

    @Override
    public String toString() {
        Log.i(TAG, "toString from message");
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setPrettyPrinting().disableHtmlEscaping().create();
        return gson.toJson(this);
    }

    public String getOriginalMessage() {
        return originalMessage;
    }

    public List<String> getEmoticons() {
        return emoticons;
    }

    public List<String> getMentions() {
        return mentions;
    }

    public List<LinkObject> getLinks() {
        return links;
    }
}
