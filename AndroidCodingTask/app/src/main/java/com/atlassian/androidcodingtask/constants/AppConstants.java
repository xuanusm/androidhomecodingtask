package com.atlassian.androidcodingtask.constants;

import java.util.regex.Pattern;

/**
 * Created by xuanusm on 7/24/2015.
 * Constant class to store global values that will be used through classes in app.
 */
public final class AppConstants {
    /**
     * Time to display splashscreen without any user action.
     */
    public static final int SPLASHSCREEN_TIME = 2000;
    /**
     * name of resource folder drawable.
     */
    public static final String DRAWBLE_RESOURCE = "drawable";
    /**
     * String separator in the app.
     */
    public static final String STRING_SEPARATOR = ",";
    /**
     * Pattern to find out emoticon text.
     */
    public static final String EMOTICON_PATTERN = "(\\()(\\w{1,15})(\\))";
    /**
     * Pattern to find out mention text.
     */
    public static final String MENTION_PATTERN = "(@)(\\w+)";

    // ******* this part related to url pattern.
    public static final String PROTOCOL_DEFAULT = "http://";
    public static final String PROTOCOL_SUPPORT = "(http|https|Http|Https|rtsp|Rtsp):\\/\\/";
    private static final String GOOD_IRI_CHAR =
            "a-zA-Z0-9\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF";

    private static final Pattern IP_ADDRESS
            = Pattern.compile(
            "((25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\\.(25[0-5]|2[0-4]"
                    + "[0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]"
                    + "[0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}"
                    + "|[1-9][0-9]|[0-9]))");
    private static final String IRI
            = "[" + GOOD_IRI_CHAR + "]([" + GOOD_IRI_CHAR + "\\-]{0,61}[" + GOOD_IRI_CHAR + "]){0,1}";

    private static final String GOOD_GTLD_CHAR =
            "a-zA-Z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF";
    private static final String GTLD = "[" + GOOD_GTLD_CHAR + "]{2,63}";
    private static final String HOST_NAME = "(" + IRI + "\\.)+" + GTLD;
    private static final Pattern DOMAIN_NAME
            = Pattern.compile("(" + HOST_NAME + "|" + IP_ADDRESS + ")");
    /**
     * Pattern to find out full web url, those start with http or https....
     */
    public static final Pattern HTTP_WEB_URL = Pattern.compile(
            "((?:" + PROTOCOL_SUPPORT + "(?:(?:[a-zA-Z0-9\\$\\-\\_\\.\\+\\!\\*\\'\\(\\)"
                    + "\\,\\;\\?\\&\\=]|(?:\\%[a-fA-F0-9]{2})){1,64}(?:\\:(?:[a-zA-Z0-9\\$\\-\\_"
                    + "\\.\\+\\!\\*\\'\\(\\)\\,\\;\\?\\&\\=]|(?:\\%[a-fA-F0-9]{2})){1,25})?\\@)?)?"
                    + "(?:" + DOMAIN_NAME + ")"
                    + "(?:\\:\\d{1,5})?)" // plus option port number
                    + "(\\/(?:(?:[" + GOOD_IRI_CHAR + "\\;\\/\\?\\:\\@\\&\\=\\#\\~"  // plus option query params
                    + "\\-\\.\\+\\!\\*\\'\\(\\)\\,\\_])|(?:\\%[a-fA-F0-9]{2}))*)?"
                    + "(?:\\b|$)");

    /**
     * Number column display emoticon on dialog.
     */
    public static final int NUMBER_EMOTICON_COLUMN = 5;

    public static final int CACHE_SIZE = 4 * 1024 * 1024; // 4MiB
}
