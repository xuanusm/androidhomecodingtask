package com.atlassian.androidcodingtask.adapters;

import android.content.Context;
import android.content.res.AssetManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.atlassian.androidcodingtask.R;
import com.atlassian.androidcodingtask.listeners.EmoticonListener;
import com.atlassian.androidcodingtask.utils.EmoticonsHelper;

import java.io.IOException;

/**
 * Created by xuanusm on 7/24/2015.
 * Adapter to display message list and its content on UI.
 */
public class EmoticonAdapter extends RecyclerView.Adapter<EmoticonHolder> {
    private static final String TAG = EmoticonAdapter.class.getCanonicalName();

    private Context context;
    private EmoticonListener clickListener;

    public EmoticonAdapter(Context context, EmoticonListener clickListener) {
        this.context = context;
        this.clickListener = clickListener;
    }

    @Override
    public EmoticonHolder onCreateViewHolder(ViewGroup parent, final int i) {
        Log.i(TAG, "onCreateViewHolder");
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.emoticon_item, parent, false);

        return new EmoticonHolder(v);
    }

    @Override
    public void onBindViewHolder(EmoticonHolder emoticonHolder, final int i) {
        Log.i(TAG, "onBindViewHolder");
        int imageResouce = EmoticonsHelper.getEmoticonResourceByName(context, EmoticonsHelper.getEmoticonList(context).get(i));
        if (imageResouce != 0) {
            emoticonHolder.getImgEmoticon().setVisibility(View.VISIBLE);
            emoticonHolder.getImgGifEmoticon().setVisibility(View.GONE);
            emoticonHolder.getImgEmoticon().setBackgroundResource(
                    EmoticonsHelper.getEmoticonResourceByName(context, EmoticonsHelper.getEmoticonList(context).get(i)));
        } else {
            emoticonHolder.getImgEmoticon().setVisibility(View.GONE);
            emoticonHolder.getImgGifEmoticon().setVisibility(View.VISIBLE);
            try {
                AssetManager am = context.getAssets();
                emoticonHolder.getImgGifEmoticon().initialize(am.open(EmoticonsHelper.getEmoticonList(context).get(i) + ".gif"));
                emoticonHolder.getImgGifEmoticon().startRendering();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        emoticonHolder.getImgEmoticon().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.displaySelectedEmoticon("(" + EmoticonsHelper.getEmoticonList(context).get(i) + ")");

            }
        });
        emoticonHolder.getImgGifEmoticon().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.displaySelectedEmoticon("(" + EmoticonsHelper.getEmoticonList(context).get(i) + ")");

            }
        });
    }

    @Override
    public int getItemCount() {
        Log.i(TAG, "getItemCount");
        return EmoticonsHelper.getEmoticonList(context).size();
    }


}
