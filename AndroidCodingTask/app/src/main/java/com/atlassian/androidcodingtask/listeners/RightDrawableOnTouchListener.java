package com.atlassian.androidcodingtask.listeners;


import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.TextView;

/**
 * Created by xuanusm on 7/25/2015.
 * Listener created to listen to touch on right drawable on edit text.
 */
public abstract class RightDrawableOnTouchListener implements OnTouchListener {
    Drawable drawable;

    public RightDrawableOnTouchListener(TextView view) {
        super();
        final Drawable[] drawables = view.getCompoundDrawables();
        if (drawables != null && drawables.length == 4)
            this.drawable = drawables[2];
    }

    /*
     * (non-Javadoc)
     * 
     * @see android.view.View.OnTouchListener#onTouch(android.view.View, android.view.MotionEvent)
     */
    public boolean onTouch(final View v, final MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN && drawable != null) {
            final int x = (int) event.getX();
            final int y = (int) event.getY();
            final Rect bounds = drawable.getBounds();
            int fuzz = 10;
            if (x >= (v.getRight() - bounds.width() - fuzz) && x <= (v.getRight() - v.getPaddingRight() + fuzz)
                    && y >= (v.getPaddingTop() - fuzz) && y <= (v.getHeight() - v.getPaddingBottom()) + fuzz) {
                drawable.setState( new int[]{ + android.R.attr.state_pressed });
            	return onDrawableTouch(event);
            }
            
        } else if ( event.getAction() == MotionEvent.ACTION_UP  && drawable != null) {
            drawable.setState( new int[]{ - android.R.attr.state_pressed });

        }
        return false;
    }

    /**
     * handle when drawable is touched by user.
     * @param event motion event that raise touch event.
     * @return value to indicate parent holder to keep handling event.
     */
    public abstract boolean onDrawableTouch(final MotionEvent event);
}