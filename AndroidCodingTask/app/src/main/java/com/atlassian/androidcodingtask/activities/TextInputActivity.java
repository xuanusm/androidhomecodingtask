package com.atlassian.androidcodingtask.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.atlassian.androidcodingtask.R;
import com.atlassian.androidcodingtask.adapters.MessageAdapter;
import com.atlassian.androidcodingtask.controls.EmoticonDialog;
import com.atlassian.androidcodingtask.listeners.EmoticonListener;
import com.atlassian.androidcodingtask.listeners.ParseListener;
import com.atlassian.androidcodingtask.listeners.RightDrawableOnTouchListener;
import com.atlassian.androidcodingtask.services.ParseContentTask;
import com.atlassian.androidcodingtask.utils.StringUtils;

/**
 * Created by xuanusm on 7/24/2015.
 * Activity to let user enter text, pass string input to parsing flow to get content data
 * and then show them back to let user know.
 */
public class TextInputActivity extends Activity implements ParseListener, EmoticonListener {
    private static final String TAG = TextInputActivity.class.getCanonicalName();

    private EditText txtInput;
    private RecyclerView lvMessages;
    private View btnEnter;
    private MessageAdapter adapter;

    private ProgressDialog progress;
    private EmoticonDialog emoticonDialog;
    private int currentCursorPosition = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_input);

        initLayoutComponent();
        initAction();
    }

    /**
     * Init layout component from content view layout of activity.
     */
    private void initLayoutComponent() {
        Log.i(TAG, "initLayoutComponent");
        txtInput = (EditText) findViewById(R.id.txtInput);
        lvMessages = (RecyclerView) findViewById(R.id.lvMessages);
        btnEnter = findViewById(R.id.btnEnter);

        adapter = new MessageAdapter(this, null);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setStackFromEnd(true);
        lvMessages.setLayoutManager(layoutManager);
        lvMessages.setAdapter(adapter);
    }

    /**
     * Init actions for UI components on screen.
     */
    private void initAction() {
        Log.i(TAG, "initAction");

        txtInput.setOnTouchListener(new RightDrawableOnTouchListener(txtInput) {
            @Override
            public boolean onDrawableTouch(final MotionEvent event) {
                currentCursorPosition = txtInput.getSelectionStart();
                displayEmoticonDialog();
                return false;
            }

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (lvMessages != null && lvMessages.getAdapter() != null && lvMessages.getAdapter().getItemCount() > 0)
                    lvMessages.smoothScrollToPosition(lvMessages.getAdapter().getItemCount() - 1);
                return super.onTouch(v, event);
            }
        });

        txtInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(android.widget.TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    handleTextInput();
                    return true;
                }
                return false;
            }

        });

        btnEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleTextInput();
            }
        });
    }

    /**
     * Display a dialog with list of emoticons so user can select to use.
     * After select, emoticon text will be insert into current cursor on composer.
     */
    private void displayEmoticonDialog() {
        if (emoticonDialog == null)
            emoticonDialog = new EmoticonDialog(TextInputActivity.this, TextInputActivity.this);
        emoticonDialog.show();
    }

    /**
     * main method to handle input text from user to find out emoticons, links and mentions.
     * This will pass text input into a seperate thread to work on it.
     */
    private void handleTextInput() {
        if (!StringUtils.isEmpty(txtInput.getText().toString())) {
            new ParseContentTask(TextInputActivity.this, adapter, TextInputActivity.this).execute(txtInput.getText().toString());
        }
    }

    /**
     * Hide soft keyboard on screeen.
     */
    private void hideKeyboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    @Override
    public void preParsing() {
        hideKeyboard();
        if (progress == null || !progress.isShowing())
            progress = ProgressDialog.show(this, this.getResources().getString(R.string.parsing),
                    this.getResources().getString(R.string.waiting), true);
    }

    @Override
    public void finishParsing() {
        txtInput.setText("");
        lvMessages.smoothScrollToPosition(lvMessages.getAdapter().getItemCount() - 1);
        if (progress != null && progress.isShowing())
            progress.dismiss();
    }

    @Override
    public String displaySelectedEmoticon(String emoticonText) {
        if (currentCursorPosition < 0)
            currentCursorPosition = txtInput.getSelectionStart();
        txtInput.getText().insert(currentCursorPosition, emoticonText);
        emoticonDialog.dismiss();
        String result = txtInput.getText().toString();
        txtInput.setSelection(result.length());
        return result;
    }
}