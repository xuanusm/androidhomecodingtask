package com.atlassian.androidcodingtask.services;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.atlassian.androidcodingtask.adapters.MessageAdapter;
import com.atlassian.androidcodingtask.listeners.ParseListener;
import com.atlassian.androidcodingtask.models.MessageContents;

/**
 * Created by xuanusm on 7/24/2015.
 * An async task created to execute parsing process from raw text into MessageContent instance.
 */
public class ParseContentTask extends AsyncTask<String, Void, MessageContents> {
    private static final String TAG = ParseContentTask.class.getCanonicalName();

    private MessageAdapter adapter;
    private ParseListener listener;
    private Context context;

    public ParseContentTask(Context context, MessageAdapter adapter, ParseListener listener) {
        this.context = context;
        this.adapter = adapter;
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        Log.i(TAG, "onPreExecute");
        listener.preParsing();
    }

    @Override
    protected MessageContents doInBackground(String... params) {
        Log.i(TAG, "doInBackground");
        return new MessageContents(context, params[0]);
    }

    @Override
    protected void onPostExecute(MessageContents messageContents) {
        Log.i(TAG, "onPostExecute");
        adapter.addMessageContent(messageContents);
        listener.finishParsing();
    }

    @Override
    protected void onCancelled() {
        Log.i(TAG, "onCancelled");
        listener.finishParsing();
    }
}
